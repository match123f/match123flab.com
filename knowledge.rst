###########
基础知识
###########

这里展示了以主题划分的关键知识框架。

.. toctree::
    :maxdepth: 2

    knowledge/social
    knowledge/identifying
    knowledge/location
    knowledge/number
    knowledge/skill
    knowledge/activity

# 社交专题

## 姓名
 **考点**：
 1. 在听力对话中，可能出现亲戚朋友等多个人名，要尤其注意**谁**做了**什么**。
 2. 在听力题中，男声和女声通常对应男名和女名。
 3. 可以用**姓氏+s**表示一家人，一个家族。（如：The Greens 格林一家）

	练习：
	 男：What do you want to be when you grow up, Gina?
	 女：I want to be a doctor. How about you, Mike?
	 男：I want to be an engineer, but my father wants me to be a scientist.
	 问：What does the boy's father want him to be?
	 答：(His father wants him to be) a scientist.

常见的英文名：
 - 女(Female)名(first name)：Alice Cindy Grace Helen Gina Jenny Mary Linda Jane Sally Kate Anna ...
 - 男(Male)名：Bob Dale Eric Frank Alan Tom Mike Jack Paul John David Bill ...
 - 姓氏(last name/family name)：Brown Miller Green Smith ...
 - 姓名前缀：Ms. 女士(不确定是否结婚)  Miss 小姐(未婚) Mrs. 夫人(已婚)   Mr. 先生 Dr. 医生/博士

你可能已经发现：一些英文姓名同时表示其他的意思（Green, green）。这个意思与实际名字无关。

一个完整的英文名结构为：
 - 名字·姓氏 (Jenny Green)。
 - 前缀 + 姓氏 (常用) /  前缀 + 名姓 (Ms. Miller / Ms. Gina Miller)

 - 将英文名翻译为中文名直接顺序翻译即可。（如：Jenny Green -> 珍妮·格林）（反之亦然）
 - 将中文名翻译为英文名可依据自身爱好决定是否调整语序。正常来讲，姓氏和名字拼音的首字母大写。特别的，有一些名人的姓名已定。（成龙 -> Jackie Chan, 吴一凡 -> Wu Yifan, 姚姚 -> Yaoyao (参见小学英语课本), 诸葛孔明 -> Zhuge Kongming）



## 问候语
课本溯源：七上 Starter Unit 1

| 原文                            | 注释                                            |
| ------------------------------- | ----------------------------------------------- |
| Hello, someone!                 | 你好，+名字!                                    |
| Good morning, someone!          | 早上好，+名字！                                 |
| Good afternoon, everyone!       | 大家下午好！                                    |
| Evening!                        | 晚上好！(非正式，不推荐)                        |
| Good night!                     | 晚安！                                          |
| Nice to meet you(, too)!        | （也）很高兴见到你！                            |
|                                 | （以上为打招呼用）                              |
| How do you do!(?)               | 近来可好～（回答也用How do you do)              |
| How are you?                    | 你过得怎样？                                    |
| I'm fine(OK), thank you.        | 我很好，谢谢。                                  |
| How's it going?                 | 事情发展得怎么样？                              |
| Not bad. / Good.                | 不坏。/很好。                                   |
|                                 | （以上为问情况用）                              |
| (Hello, )I'm(am) someone.       | （你好，）我是+名字。                           |
| What's your name?               | 你的名字是？ (What's = What is)                 |
| What's your (first/last) name?  | 你（名谁/姓甚）？（family name=last name)       |
| Who's he(she)?                  | 他/她是谁？                                     |
| One's name is someone.          | 某人的名字是+名字。                             |
| (My name's) Tom.                | (我的名字是)Tom。（name's = name is)            | 
| I'm Tom.                        | 我是Tom。（sb. be +名字）                       |
| (Is he/she)/Are you someone?    | 他/她/你是+名字 吗？                            |
| No, he isn't./No, I'm not.      | 不，他/我不是。(缩写:isn't = is not, I am= I'm) |
|                                 | （以上为问名字用）                              |
| What's your (tele)phone number? | 你的电话号码是？                                |
| xxx-xxxx.                       | （要一位一位的读，在连字符处略停顿。）          |
| xxx-xx22. (xxx, xx double two.) | （号码中，double 表示“两个”）                   |
| This is someone (speaking).     | 我（说话的人）是+名字。                         |
| Who's that?                     | 你（对方）是谁？                                |
| Is that someone speaking?       | 你（对方）是+名字吗？                           |
| Sorry， someone is not in.      | 对不起，某人不在。                              |
| Hold on, please.                | 等一下。（叫要找的人接电话，即人是在的）        |
|                                 | （以上为电话用语）                              |
| How do you spell it?            | 你怎样拼写（这个单词）？                        |
| S-P-E-L-L(, spell).             | (一个字母一个字母的读出来。）                   |

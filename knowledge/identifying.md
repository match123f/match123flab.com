# 辨别专题

## 器具
常见器具有：
 - 建筑物有关： floor 地板 wall 墙 door 门🚪 key 钥匙🔑（a set of keys:key 一套钥匙）lock 锁🔒
 - 家具：(_quilt 被褥_ bed 床🛏 _pillow 枕头_) desk/table 桌 sofa 沙发 chair 椅子🪑 bookcase/bookshelf 书架
 - 现代产物：
	 - 时间(time)：clock 时钟🕐 alarm 闹钟⏰ watch 手表⌚ 
	 - **信息技术(IT information technology)：TV 电视机📺 radio 收音机📻/tape player 磁带播放机 robot 机器人 computer 电脑🖥/computer game 游戏机🎮/(notebook可表笔记本电脑💻) **
	 - 信息载体：(书籍见下) paper 纸📰/🧻(a piece of paper 一张纸)(napkin 餐巾/纸巾  photo 相片 picture 图片🖼  CD/DVD 光盘💿📀 tape 磁带
	 - light 灯/光源：lantern灯笼🏮/flashlight 手电🔦/traffic light 交通信号灯🚥 
 - 工作，学习用具：cup 杯子🥤 map 地图🗺 ID card 身份证/学生卡/身份识别卡 (非课标: credit 信用卡💳) 
	 - book 书📚 notebook 笔记本📓 textbook 课本 story book 故事书 novel/fiction 小说 dictionary 字典 newspaper 报纸 magazine 杂志 report 报告 
	 - ruler 尺子📏 pen 钢笔🖋（penpal 笔友）pencil 铅笔✏ eraser 擦头 pencil box(case) 文具盒 
 - 服饰/随身：hat 帽子👒/ cap 鸭舌帽🧢 school bag 书包/handbag 手提包👜/backpack 背包

## 颜色
常见颜色有：
 - red 🔴
 - orange 🟠
 - yellow 🟡
 - green 🟢
 - blue 🔵
 - purple 🟣
 - brown 🟤
 - black ⚫
 - white ⚪

颜色词语的表意：
1. 做形容词，修饰名词。比如：青苹果 🍏 green apple
2. 做名词。

	练习
	请说出下列图片的英文。
	1. 🛏 2. 🗺 3.⏰ (注意和bell 🔔 区分) 4.🎈 5. 🪁 6. 🎁 7. 📚 8. ✏ 9. 🖊 10. 🔑11. 🚪12. 🛏 13. 🪑14. 🚿 15. ✂
	
	请用适合的短语翻译下列内容。
	颜色类：1. 🚗 2. 📘 3.🍏 4. 🖋 5.(黑色的夹克衫) 6.(紫色的衬衫) 7. (橙色的字母“Z”) 8. (白色的气球)
	类型类：9.(中英词典) 10. (科幻小说) 11. (恐怖片) 12. (纪录片) 13. (书包) 14.(Kate的房间很整洁)
	方位类：14.(在桌子上) 15. (在椅子下) 16.(在教室里)
	  

## 亲属
常见亲属用词有：
 - 子辈：son 儿子 daughter 女儿 granddaughter 孙女（只读一个"d"）
 - 同辈：sister **亲**姐妹  brother **亲**兄弟  cousin **堂**兄弟姊妹 wife 妻子 husband 丈夫
 - 父母辈：mother/mum 妈妈 father/dad 爸爸 （父母 parents）aunt 阿姨 uncle 叔叔
 - 祖辈：grandfather/grandpa 爷爷/外公 grandmother/grandma 奶奶/外婆（"d" 不发音）
 - 与方位有关：penpal 笔友 neighbor 邻居（neighborhood 街区）classmate 同学 teammate 队友
 - 与利益有关：friend/pal 朋友 stranger 陌生人 enemy 敌人

	用例：Who are they in the picture?
	--These are my parents. Those are my grandparents.


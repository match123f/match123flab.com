# 活动专题

常见活动如下表：
| 原文                                                           | 注释                                             |
| -------------------------------------------------------------- | ------------------------------------------------ |
| (Lost and Found)                                               | （失物招领）                                     |
| I lost my xxx.                                                 | 我丢失了我的xxx。                                |
| Is it yours?/Are they yours?                                   | 这（些）是你的吗？                               |
| Ask the teacher for it./Call me at xxx./Email me at xxx@xxx.x. | 找老师/拨打电话/发邮件认领。（@ = at, "." = dot) |
|                                                                |                                                  |

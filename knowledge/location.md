# 方位专题

## 方位指代

常见的有方位意义的代词有：
 - here (这) (nere here 这附近)/ there (那)
 - this, these (这) / that, those (那)
	 - There/Those 是复数，this/that 是单数

常见的方位介词有：
 - 小范围：on 在...上面  under 在...下面  in 在...(房间/空间)里面
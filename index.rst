.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. ERROR::
   该项目已被废弃，并宣布为无限期停止更新。该项目暂无志愿者接手。

--------------------------------------
欢迎来到 一片废墟 手册！
--------------------------------------

工程进度
~~~~~~~~~~~~~~~~~~~

更新记录：
   - 2023-3-11 添加了Starter Units部分（识别、社交）
   - 2023-3-12 添加了七上U1-4部分（识别器具，失物招领活动，小范围方位，号码读数，姓名前缀）

+------+------+------+------+------+------+
| 单元 | 七上 | 七下 | 八上 | 八下 | 九全 |
+------+------+------+------+------+------+
| U01  | ✔    | ⏳   | ⏳   | ⏳   | ⏳   |
+------+------+------+------+------+------+
| U02  | ✔    | ⏳   | ⏳   | ⏳   | ⏳   |
+------+------+------+------+------+------+
| U03  | ✔    | ⏳   | ⏳   | ⏳   | ⏳   |
+------+------+------+------+------+------+
| U04  | ✔    | ⏳   | ⏳   | ⏳   | ⏳   |
+------+------+------+------+------+------+
| U05  | ⏳   | ⏳   | ⏳   | ⏳   | ⏳   |
+------+------+------+------+------+------+
| U06  | ⏳   | ⏳   | ⏳   | ⏳   | ⏳   |
+------+------+------+------+------+------+
| U07  | ⏳   | ⏳   | ⏳   | ⏳   | ⏳   |
+------+------+------+------+------+------+
| U08  | ⏳   | ⏳   | ⏳   | ⏳   | ⏳   |
+------+------+------+------+------+------+
| U09  | ⏳   | ⏳   | ⏳   | ⏳   | ⏳   |
+------+------+------+------+------+------+
| U10  | ❌   | ⏳   | ⏳   | ⏳   | ⏳   |
+------+------+------+------+------+------+
| U11  | ❌   | ⏳   | ❌   | ❌   | ⏳   |
+------+------+------+------+------+------+
| U12  | ❌   | ⏳   | ❌   | ❌   | ⏳   |
+------+------+------+------+------+------+
| U13  | ❌   | ❌   | ❌   | ❌   | ⏳   |
+------+------+------+------+------+------+
| U14  | ❌   | ❌   | ❌   | ❌   | ⏳   |
+------+------+------+------+------+------+

.. toctree::
   :maxdepth: 2
   :caption: 目录

   roadmap
   knowledge
   speaking_skills

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

关注该项目
~~~~~~~~~~~

* https://gitlab.com/match123f/match123f.gitlab.io
